package com.example.teb_demo

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.time.Duration

class MainActivity : AppCompatActivity() {
    val TAG = "TEB-activity"
    val USERNAME_KEY = "USERNAME"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }


    fun onClick(view : View){

        val name = username.text.toString()

        //Intent(nadawca, odbiorca)
        val intent = Intent(this, UserDock::class.java).apply {

            putExtra(USERNAME_KEY, name)

        }
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            1 -> {
                if(resultCode == Activity.RESULT_OK){
                    Log.d("TEB", "${data == null}")
                    val message = data?.getStringExtra("email-count")
                    Toast.makeText(this, "$message", Toast.LENGTH_LONG).show()
                    return
                }
                Toast.makeText(this, "User aborted", Toast.LENGTH_SHORT).show()
            }
        }
    }


}