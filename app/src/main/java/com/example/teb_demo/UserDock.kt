package com.example.teb_demo

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_user_dock.*

class UserDock : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_dock)

        val message = intent.getStringExtra("USERNAME")

        Log.d("UserDock", "${intent == null}")
        Log.d("UserDock", message)

        username.text = message

        email_button.setOnClickListener {
            val intent = Intent().apply { putExtra("email-count", "42") }
            setResult(Activity.RESULT_OK, intent)
            Log.d("TEB", "finishing activity")
            finish()
        }
    }


}
